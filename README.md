## Prostředí

### Vybrané prostředí pro obhajobu je Ubuntu 20.04 - 64bit z důvodu popsaného v odseku Windows - Limitace

# Ubuntu 20.04 - 64bit

### Instalace

Jsou zapotřebí práva uživatele root

* Stažení .deb balíčku

```
wget https://www.dropbox.com/s/jvnem7p6nvdmxwj/Calculator_1.0-1_ubuntu_amd64.deb
```

Nebo z archívu odevzdaného na serveru

* Instalace .deb balíčku

```
sudo dpkg -i Calculator_1.0-1_ubuntu_amd64.deb
```

* Ověření instalace

```
dpkg-query -l | grep "calculator"
```

* Program je nainstalován v:

```
/opt/Calculator
```

### Odinstalace

```
sudo dpkg -r calculator
```

### Spouštení

* Instalace balíčku vytvoří odkaz v hlavním menu Ubuntu a je jej možné vyhledat názvem

```
IVS Calculator
```

## Windows 64bit

### Limitace

* Ke běhu programu je zapotřebí grafického ovladače podporujícího minimálně OpenGL 2.0 proto program není možné snadne spustit na virtualizovaném Windows který obsahuje základní grafické ovladače s OpenGL 1.0, bez přídavné instalace nebo aktualizace grafických ovladačů, tyto kroky jsou nám však v čase odevzdávaní neznáme proto program není možné předvést při obhajobách na této platformě ve **virtualizovaném** prostředí.

### Instalace

* Stažení instalátoru z

<[https://www.dropbox.com/s/mlkljrssgl5nvx2/Calculator-1.0-1 x64 setup.exe](https://www.dropbox.com/s/mlkljrssgl5nvx2/Calculator-1.0-1%20x64%20setup.exe)>

Nebo z archívu odevzdaného na serveru

* Instalátor vás provede všemi potřebnými kroky, nebo odkazujte na uživatelskou příručku

### Odinstalace

* Odinstalace je možná spustením odinštalačního programu umístneného ve složce kam byl program nainstalován, nebo ho lze odinstalovat z aplikačního manažeru Windows pod názvem

```
Calculator version <release>
```

Popsané v uživatelské příručce

### Spouštení

* Pokud jste zvolili možnost vytvoření zástupce na ploše, je možné aplikaci spustit pomocí tohoto zástupce, pokud jste tuto možnost nezvolili, program třeba spustit ze složky kde byl nainstalován, pokud třeba, tento krok je popsán v příručce.

# Vytvoření spustitelného souboru

* Pro vytvoření spustitelného souboru je třeba mít nainstalován python verze 3, doporučovaná verze 3.8 nebo 3.9 a python package manager - pip

* Makefile očekáva že python lze spustit příkazem "python3", pokud váš python spouštíte jinak, je třeba vytvořit alias, nebo upravit hodnotu na prvním řádku v makefile aby odpovídala vašemu aliasu v PATH vaše příkazové řádky

* Je také nutno nainstalovat příslušné baličky popsané v "requirements.txt" ve složce "src/" příkazem

```
pip install -r requirements.py
```

* A mít nainstalován balíček PyInstaller, pomocí příkazu
```
pip install pyinstaller
```

* Spustitelné soubory lze vytvořit pomocí Makefile obsaženého ve složce

```
src/
```

* V této složce lze spustit příkaz pro vytvoření spustitelného souboru kalkulačky i souboru pro výpočet výběrové směrodatné odchylky

```
pro všechny programy
make
nebo
make all

pro jednotlivé programy
make compile
nebo
make profile
```

* Tento make target vytvoří

```
src/build/

src/dist/Calculator/

src/dist/profiler nebo /src/dist/profiler.exe v závislosti od systému
```

* Spustitelné soubory pro oba programy jsou umístneny ve složce:

```
src/dist/
```

* Příkazem

```
make clean
```

Je možné tyto složky vyčistit - tento make target lze provést pouze na platformě Ubuntu

## Autori

#### Fallen Frogs

xbalaz14 Dáša Balážová @dbvl

xplach10 Ivan Plachý @SpectruM9

xpatuc00 Samuel Patúc @Hydridity

## Licence

Program je poskytovan pod licencí **GNU Affero General Public License v3.0**

Po skončení projektu bude repositář odemčen pro veřejnost a případne archivován.

[![License](https://img.shields.io/badge/license-GPL%20License%20v3-blue.svg)](LICENSE.md)
