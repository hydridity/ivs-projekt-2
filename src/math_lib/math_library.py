import math


def add(*args):
    """!
    Add together all function arguments
    {@code result = add(1,2)}
    {@code result = add(1,2,30.5)}
    {@code
    data = (1,2,5)
    result = add(*data)}

    @param *args Float or Int: Arguments to add

    @return Return Float or Int
    """
    return sum(args)


def sub(*args):
    """!
    Subtracts from all function arguments
    {@code result = sub(10,20)}
    {@code result = sub(10,20,30.5)}
    {@code
    data = (1,5,3)
    result = sub(*data)}

    @param *args Float or Int: Arguments to subtract

    @return Return Float or Int
    """
    total = args[0]  # subtract from first argument
    # tuples are immutable, start subtracting every number after first element
    for number in args[1:]:
        total -= number
    return total


def mul(*args):
    """!
    Multiplies all function arguments
    {@code result = mul(1,2)}
    {@code result = sub(1,2,3)}
    {@code
    data = (1,10,3)
    result = mul(*data)}

    @param *args Float or Int: Arguments to multiply

    @return Return Float or Int
    """
    total = args[0]  # subtract from first argument
    # tuples are immutable, start subtracting every number after first element
    for number in args[1:]:
        total *= number
    return total


def div(*args):
    """!
        Divides all function arguments
        {@code result = div(10,20)}
        {@code result = div(10,20,30.5)}
        {@code
        data = (1,5,3)
        result = div(*data)}

        @param *args Float or Int: Arguments to divide
        @return Return Float or Int
        @throws ZeroDivisionError when one of arguments is 0
        """
    total = args[0]  # subtract from first argument
    # tuples are immutable, start subtracting every number after first element
    for number in args[1:]:
        total /= number
    return total


def fact(number):
    """!
    Calculate factorial from argument
    {@code result = fact(5)}

    @param number Float or Int
    @return Return Float or Int
    @throws OverflowError when result is over interpreter's double memory allocation
    """
    return math.gamma(number + 1)


def power(*args):
    """!
    Exponentiate all function arguments
    {@code result = power(10,20)}
    {@code result = power(10,20,30.5)}
    {@code
    data = (1,5,3)
    result = power(*data)}

    @param *args Float or Int: Arguments to exponentiate
    @return Return Float or Int
    """
    total = args[0]  # subtract from first argument
    if len(args) > 1:
        # tuples are immutable, start subtracting
        # every number after first element
        for number in args[1:]:
            total = math.pow(total, number)
    else:
        total = math.pow(total, 2)
    return total


def sqrt(number):
    """!
    Calculate square root of argument
    {@code result = sqrt(10)}

    @param number Float or Int: Arguments to exponentiate
    @return Return Float or Int
    @throws ValueError when calculating square root of negative number
    """
    return math.sqrt(number)
