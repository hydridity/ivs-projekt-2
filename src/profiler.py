from math_lib import *
import fileinput


def profile(data):
    priemer = (1/len(data))*add(*data)

    mocnina = []
    for x in data:
        mocnina.append(power(x))

    suma = add(*tuple(mocnina))

    zatvorky = suma - len(data)*power(priemer)

    # variacia = (1/(len(set)-1)) * zatvorky # gives error > 0,2
    variacia = zatvorky / len(data)

    sdodchylka = sqrt(variacia)  # smerodajna odchylka
    return sdodchylka


if __name__ == "__main__":
    dataset = []
    for line in fileinput.input():
        try:
            number = float(line.rstrip())
            dataset.append(number)
        except ValueError:
            print(f"Invalid data, item: {line.rstrip()} is not valid number")

    sdodchylka = profile(tuple(dataset))

    print(sdodchylka)