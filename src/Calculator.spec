# -*- mode: python ; coding: utf-8 -*-
import sys
from kivymd import hooks_path as kivymd_hooks_path

if sys.platform.startswith('linux'):
	block_cipher = None


	a = Analysis(['Calculator.py'],
		     binaries=[],
		     datas=[('calculator.kv', '.')],
		     hiddenimports=[],
		     hookspath=[kivymd_hooks_path],
		     runtime_hooks=[],
		     excludes=[],
		     win_no_prefer_redirects=False,
		     win_private_assemblies=False,
		     cipher=block_cipher,
		     noarchive=False)
	pyz = PYZ(a.pure, a.zipped_data,
		     cipher=block_cipher)
	exe = EXE(pyz,
		  a.scripts,
		  [],
		  exclude_binaries=True,
		  name='Calculator',
		  debug=False,
		  bootloader_ignore_signals=False,
		  strip=False,
		  upx=True,
		  console=True )
	coll = COLLECT(exe,
		       a.binaries,
		       a.zipfiles,
		       a.datas,
		       strip=False,
		       upx=True,
		       upx_exclude=[],
		       name='Calculator')
elif sys.platform.startswith('win32'):
	# -*- mode: python ; coding: utf-8 -*-
	from kivy_deps import sdl2, glew
	from kivymd import hooks_path as kivymd_hooks_path


	block_cipher = None


	a = Analysis(['Calculator.py'],
		     binaries=[],
		     datas=[('calculator.kv', '.')],
		     hiddenimports=[],
		     hookspath=[kivymd_hooks_path],
		     runtime_hooks=[],
		     excludes=[],
		     win_no_prefer_redirects=False,
		     win_private_assemblies=False,
		     cipher=block_cipher,
		     noarchive=False)
	pyz = PYZ(a.pure, a.zipped_data,
		     cipher=block_cipher)
	exe = EXE(pyz,
		  a.scripts,
		  [],
		  exclude_binaries=True,
		  name='Calculator',
		  debug=False,
		  bootloader_ignore_signals=False,
		  strip=False,
		  upx=True,
		  console=False )
	coll = COLLECT(exe,
		       a.binaries,
		       a.zipfiles,
		       a.datas,
		       *[Tree(p) for p in (sdl2.dep_bins + glew.dep_bins)],
		       strip=False,
		       upx=True,
		       upx_exclude=[],
		       name='Calculator')
