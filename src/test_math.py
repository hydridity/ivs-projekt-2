import unittest
from math_lib import *


class TestSum(unittest.TestCase):
    def test_simple(self):
        data = (1, 2)
        result = add(*data)
        self.assertEqual(result, 3)

    def test_combined(self):
        data = (1, -2)
        result = add(*data)
        self.assertEqual(result, -1)

    def test_multiple(self):
        data = (1, -2, -3, 5, 6)
        result = add(*data)
        self.assertEqual(result, 7)

    def test_negative(self):
        data = (-1, -2, -3, -4, -5)
        result = add(*data)
        self.assertEqual(result, -15)


class TestSub(unittest.TestCase):
    def test_simple(self):
        data = (-1, -2)
        result = sub(*data)
        self.assertEqual(result, 1)

    def test_combined(self):
        data = (-1, 2)
        result = sub(*data)
        self.assertEqual(result, -3)

    def test_multiple(self):
        data = (-1, 2, -3, 4, -5)
        result = sub(*data)
        self.assertEqual(result, 1)


class TestMul(unittest.TestCase):
    def test_simple(self):
        data = (1, 2)
        result = mul(*data)
        self.assertEqual(result, 2)

    def test_combined(self):
        data = (1, -2)
        result = mul(*data)
        self.assertEqual(result, -2)

    def test_multiple(self):
        data = (1, -2, 3)
        result = mul(*data)
        self.assertEqual(result, -6)


class TestDiv(unittest.TestCase):
    def test_simple(self):
        data = (1, 2)
        result = div(*data)
        self.assertEqual(result, 0.5)

    def test_combined(self):
        data = (1, -2)
        result = div(*data)
        self.assertEqual(result, -0.5)

    def test_multiple(self):
        data = (3, -2, 1)
        result = div(*data)
        self.assertEqual(result, -1.5)

    def test_zero(self):
        with self.assertRaises(ZeroDivisionError):
            data = (1, 0)
            result = div(*data)

    def test_zero_multiple(self):
        with self.assertRaises(ZeroDivisionError):
            data = (3, -2, 1, 0)
            result = div(*data)


class TestFactorial(unittest.TestCase):
    def test_simple(self):
        data = 3
        result = fact(data)
        self.assertEqual(result, 6)

    def test_multiple(self):
        with self.assertRaises(TypeError):
            data = (3, 6)
            result = fact(data)


class TestPower(unittest.TestCase):
    def test_simple(self):
        data = (3, 6)
        result = power(*data)
        self.assertEqual(result, 729)

    def test_zero_exponent(self):
        data = (5, 0)
        result = power(*data)
        self.assertEqual(result, 1)

    def test_zero_base(self):
        data = (0, 5)
        result = power(*data)
        self.assertEqual(result, 0)

    def test_multiple(self):
        data = (3, 6, 2)
        result = power(*data)
        self.assertEqual(result, 531441)


class TestSqrt(unittest.TestCase):
    def test_simple(self):
        data = 25
        result = sqrt(data)
        self.assertEqual(result, 5)

    def test_negative(self):
        with self.assertRaises(ValueError):
            data = -5
            result = sqrt(data)


if __name__ == '__main__':
    unittest.main()
