from kivymd.app import MDApp
from kivymd.uix.gridlayout import GridLayout
from kivymd.uix.boxlayout import BoxLayout
from kivymd.uix.dialog import MDDialog
from kivymd.uix.label import MDLabel
from kivymd.uix.button import MDFlatButton
from kivy.uix.widget import Widget
from kivy.core.window import Window
import math_lib


class KeyboardListener(Widget):
    # https://kivy.org/doc/stable/api-kivy.core.window.html?highlight=keyboard#kivy.core.window.Keyboard
    # modifikovaný example

    def __init__(self, calculator):
        super(KeyboardListener, self).__init__()
        self.calculator = calculator
        self._keyboard = Window.request_keyboard(
            None, self, 'text')
        self._keyboard.bind(on_key_down=self._on_keyboard_down)
        self.buttons = self._get_buttons()

    def _get_buttons(self):
        # Vygeneruje tuple tlačitok na obrazovke
        layout = tuple(item for item in self.calculator.walk()
                       if not
                       isinstance(item, (BoxLayout, GridLayout, MDLabel)))
        """ Tuple comprehension bol boužitý,
        pretože tuple je treba vygenerovat celú na mieste, ekvivalent kódu:
        layout = []
        for item in self.calculator.walk():
            if not isinstance(item, (BoxLayout, GridLayout, MDLabel):
                layout.append(item)
        layout = tuple(layout)
        """
        return layout

    def _on_keyboard_down(self, keyboard, keycode, text, modifiers):
        key = keycode[1].replace("numpad", "")  # odstrani numpad prefix
        for button in self.buttons:
            if button.text == key:
                button.trigger_action()
            elif key == "substract" and button.text == "-":
                button.trigger_action()
            elif key == "add" and button.text == "+":
                button.trigger_action()
            elif key == "mul" and button.text == "*":
                button.trigger_action()
            elif key == "divide" and button.text == "/":
                button.trigger_action()
            elif key == "enter" and button.text == "=":
                button.trigger_action()

        # Return True to accept the key. Otherwise, it will be used by
        # the system.
        return True


class CalculationGrid(GridLayout):

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        self.active_operator = ""
        self.result = ""
        self.operands = []
        self.dialog = None
        self._keyboard = None

    def serve_keyboard(self):
        self._keyboard = KeyboardListener(self)
        # referencia calculator grid v keyboard listener

    def operation(self, data, operator):
        if self.result != "":
            self.clear()
            self.operands.append(float(self.result))
            self.active_operator = operator
            if operator == "√":
                self.display.text = operator + self.display.text
            else:
                self.display.text += operator
            self.result = ""
            return

        if self.active_operator == "":
            if data == "":
                self.operands.append(float(0))
                self.display.text = "0"
            else:
                self.operands.append(float(data))
            self.active_operator = operator
            if operator == "√":
                self.display.text = operator + self.display.text
            else:
                self.display.text += operator
        elif self.active_operator != operator and operator != "=":
            MDApp.get_running_app().error("This version of calculator allows "
                                          "only one operator per calculation")
        elif operator == "=":
            if self.active_operator == "!":
                next_operand = data.split(self.active_operator)[0]
            else:
                next_operand = data.split(self.active_operator)[-1]
            self.operands.append(float(next_operand))
            self.equals()
        else:
            if self.display.text[-1] != self.active_operator:
                self.display.text += operator
                next_operand = data.split(self.active_operator)[-1]
                self.operands.append(float(next_operand))

    # Function called when equals is pressed
    def equals(self):
        try:
            if self.active_operator == "+":
                self.result = str(math_lib.add(*tuple(self.operands)))
            elif self.active_operator == "-":
                self.result = str(math_lib.sub(*tuple(self.operands)))
            elif self.active_operator == "*":
                self.result = str(math_lib.mul(*tuple(self.operands)))
            elif self.active_operator == "/":
                self.result = str(math_lib.div(*tuple(self.operands)))
            elif self.active_operator == "^":
                self.result = str(math_lib.power(*tuple(self.operands)))
            elif self.active_operator == "√":
                self.result = str(math_lib.sqrt(self.operands[0]))
            elif self.active_operator == "!":
                self.result = str(math_lib.fact(self.operands[0]))

            self.display.text += "=" + '%g' % (float(self.result))
        except ZeroDivisionError:
            MDApp.get_running_app().error("ERROR: Division by zero")
        except OverflowError:
            MDApp.get_running_app().error("ERROR: Overflow,"
                                          " result is too large")
        except Exception:
            MDApp.get_running_app().error("General Error")

    def clear(self, ac=False):
        self.active_operator = ""
        self.operands = []
        if ac:
            self.result = ""
            self.display.text = ""
        else:
            self.display.text = '%g' % (float(self.result))

    def info(self):
        MDApp.get_running_app().error("K ovládání kalkulačky je možné použít"
                                      " numerickou klávesnici a tlačítka"
                                      " v aplikaci.\n"
                                      "Pro operátory +-*/^ se výpočet zadává"
                                      " ve tvaru 'x <operátor> y'.\n"
                                      "Operátory ! a √ se používají "
                                      "ve tvaru 'x <operátor>'.\n"
                                      "Nelze použít více různých operátorů"
                                      " při jednom výpočtu "
                                      "- aplikace vypíše chybu.\n"
                                      "Po zmáčknutí = nebo ENTER "
                                      "na klávesnici se vypočte výsledek,"
                                      " se kterým lze dále pracovat.")


class CalculatorApp(MDApp):
    dialog = None

    def build(self):
        app = CalculationGrid()
        app.serve_keyboard()
        self.theme_cls.theme_style = "Dark"  # Dark theme is a go
        return app

    def error(self, error):
        button = MDFlatButton(text="CONFIRM",
                              text_color=self.theme_cls.primary_color)
        self.dialog = MDDialog(
            text=error,
            buttons=[button]
        )
        button.bind(on_press=self.dialog.dismiss)
        self.dialog.open()


if __name__ == "__main__":
    CalculatorApp().run()
